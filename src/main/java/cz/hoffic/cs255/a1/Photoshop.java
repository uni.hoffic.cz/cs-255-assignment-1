package cz.hoffic.cs255.a1;

import cz.hoffic.cs255.a1.action.ContrastAction;
import cz.hoffic.cs255.a1.action.CrossCorrelationAction;
import cz.hoffic.cs255.a1.action.GammaAction;
import cz.hoffic.cs255.a1.action.GreyScaleAction;
import cz.hoffic.cs255.a1.action.HistogramAction;
import cz.hoffic.cs255.a1.action.HistogramEqualisationAction;
import cz.hoffic.cs255.a1.action.InvertAction;
import cz.hoffic.cs255.a1.action.ResetAction;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class Photoshop extends Application {

  public static final String FILE_NAME = "raytrace.jpg";

  @Override
  public void start(Stage stage) throws FileNotFoundException {
    stage.setTitle("Photoshop");

    Image image = new Image(new FileInputStream(FILE_NAME));

    ImageView imageView = new ImageView(image);

    Button resetButton = new Button("Reset");
    Button greyScaleButton = new Button("Grey Scale");
    Button invertButton = new Button("Invert");
    Button gammaButton = new Button("Gamma Correct");
    Button contrastButton = new Button("Contrast Stretching");
    Button histogramButton = new Button("Histograms");
    Button histogramEqualisationButton = new Button("Histogram Equalisation");
    Button ccButton = new Button("Cross Correlation");

    new ResetAction(resetButton, imageView);
    new GreyScaleAction(greyScaleButton, imageView);
    new InvertAction(invertButton, imageView);
    new GammaAction(gammaButton, imageView);
    new ContrastAction(contrastButton, imageView);
    new HistogramAction(histogramButton, imageView);
    new HistogramEqualisationAction(histogramEqualisationButton, imageView);
    new CrossCorrelationAction(ccButton, imageView);

    FlowPane root = new FlowPane();
    root.setVgap(10);
    root.setHgap(5);

    root.getChildren().addAll(
        resetButton,
        greyScaleButton,
        invertButton,
        gammaButton,
        contrastButton,
        histogramButton,
        histogramEqualisationButton,
        ccButton,
        imageView
    );

    Scene scene = new Scene(root, 1024, 1024);
    stage.setScene(scene);
    stage.show();
  }

  public static void main(String[] args) {
    launch();
  }
}
