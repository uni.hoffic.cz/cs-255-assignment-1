package cz.hoffic.cs255.a1.action;

import cz.hoffic.cs255.a1.helper.HistogramHelper;
import cz.hoffic.cs255.a1.helper.HistogramHelper.ChannelType;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.image.ImageView;

public class HistogramAction {

  public HistogramAction(Button triggerButton, ImageView imageView) {
    triggerButton.setOnAction(event -> {

      List<String> choices = new LinkedList<>();
      choices.add(ChannelType.RED.toString());
      choices.add(ChannelType.GREEN.toString());
      choices.add(ChannelType.BLUE.toString());
      choices.add(ChannelType.BRIGHTNESS.toString());

      ChoiceDialog<String> dialog = new ChoiceDialog<>(ChannelType.BRIGHTNESS.toString(), choices);
      dialog.setTitle("Select a channel");
      dialog.setHeaderText("Select a channel");
      dialog.setContentText("Channel:");

      Optional<String> result = dialog.showAndWait();
      if (result.isPresent()) {
        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("Input value");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLowerBound(0);
        yAxis.setLabel("Frequency");

        BarChart<String, Number> barChart = new BarChart<>(xAxis, yAxis);
        barChart.setCategoryGap(0);
        barChart.setBarGap(0);

        Series chartSeries = new Series();
        chartSeries.setName("Value distribution");

        int[] distribution = HistogramHelper.getHistogramDistribution(
            imageView,
            ChannelType.valueOf(result.get())
        );

        for (int i = 0; i < distribution.length; i++) {
          chartSeries.getData().add(
              new XYChart.Data(
                  Integer.toString(i),
                  distribution[i]
              )
          );
        }

        barChart.getData().add(chartSeries);

        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setHeaderText("Channel: " + result.get());
        alert.getDialogPane().setContent(barChart);
        alert.showAndWait();
      }
    });
  }
}
