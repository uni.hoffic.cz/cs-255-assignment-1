package cz.hoffic.cs255.a1.action;

import cz.hoffic.cs255.a1.ImageProcessor;
import cz.hoffic.cs255.a1.lookuptable.InvertLookupTable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class GreyScaleAction {

  public GreyScaleAction(Button triggerButton, ImageView imageView) {
    triggerButton.setOnAction(event -> {
      System.out.println("Grey scale.");

      Image greyScaleImage = ImageProcessor.greyScaleImgge(
          imageView.getImage()
      );

      imageView.setImage(greyScaleImage);
    });
  }
}
