package cz.hoffic.cs255.a1.action;

import cz.hoffic.cs255.a1.ImageProcessor;
import cz.hoffic.cs255.a1.matrix.KernelMatrix;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class CrossCorrelationAction {

  public CrossCorrelationAction(Button triggerButton, ImageView imageView) {
    triggerButton.setOnAction(event -> {
      System.out.println("Cross correlation.");

      Image processedImage = ImageProcessor.processImage(
          imageView.getImage(),
          KernelMatrix.getLaplacian()
      );

      imageView.setImage(processedImage);
    });
  }
}
