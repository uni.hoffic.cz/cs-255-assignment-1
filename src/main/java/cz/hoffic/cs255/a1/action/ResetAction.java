package cz.hoffic.cs255.a1.action;

import cz.hoffic.cs255.a1.Photoshop;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ResetAction {

  public ResetAction(Button triggerButton, ImageView imageView) {
    triggerButton.setOnAction(event -> {
      System.out.println("Reset.");

      Image image = null;

      try {
        image = new Image(new FileInputStream(Photoshop.FILE_NAME));
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      }

      imageView.setImage(image);
    });
  }
}
