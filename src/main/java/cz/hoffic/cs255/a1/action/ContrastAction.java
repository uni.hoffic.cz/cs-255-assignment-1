package cz.hoffic.cs255.a1.action;

import cz.hoffic.cs255.a1.ImageProcessor;
import cz.hoffic.cs255.a1.lookuptable.ContrastLookupTable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javafx.geometry.Point2D;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Pair;

public class ContrastAction {

  public ContrastAction(Button triggerButton, ImageView imageView) {
    triggerButton.setOnAction(event -> {
      NumberAxis xAxis = new NumberAxis(0.0, 255.0, 10);
      xAxis.setLabel("Input value");

      NumberAxis yAxis = new NumberAxis(0.0, 255.0, 10);
      yAxis.setLabel("Output value");

      LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);
      Series chartSeries = getBlankSeries(lineChart);

      ArrayList<Pair<Integer, Integer>> thresholds = new ArrayList<>();

      addPoint(0.0, 0.0, chartSeries, thresholds);
      addPoint(255.0, 255.0, chartSeries, thresholds);

      lineChart.setOnMouseClicked(clickEvent -> {
        Point2D mouseSceneCoords = new Point2D(clickEvent.getSceneX(), clickEvent.getSceneY());
        double x = xAxis.sceneToLocal(mouseSceneCoords).getX();
        double y = yAxis.sceneToLocal(mouseSceneCoords).getY();

        int localX = (int) Math.min(255.0, (double) xAxis.getValueForDisplay(x));
        int localY = (int) Math.min(255.0, (double) yAxis.getValueForDisplay(y));

        addPoint(localX, localY, chartSeries, thresholds);
      });

      Alert alert = new Alert(AlertType.INFORMATION);
      alert.getDialogPane().setContent(lineChart);
      alert.showAndWait();

      thresholds.sort(Comparator.comparingInt(Pair::getKey));

      Image gammaCorrectedImage = ImageProcessor.processImage(
          imageView.getImage(),
          new ContrastLookupTable(
              thresholds
          )
      );

      imageView.setImage(gammaCorrectedImage);
    });
  }

  private Series getBlankSeries(LineChart chart) {
    Series chartSeries = new Series();
    chartSeries.setName("Contrast Curve");
    chart.getData().add(chartSeries);

    return chartSeries;
  }

  private void addPoint(double key, double value, Series chartSeries, List thresholds) {
    thresholds.add(new Pair<>(key, value));
    chartSeries.getData().add(new Data<>(key, value));
  }
}
