package cz.hoffic.cs255.a1.action;

import cz.hoffic.cs255.a1.ImageProcessor;
import cz.hoffic.cs255.a1.lookuptable.GammaCorrectionLookupTable;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class GammaAction {
  public GammaAction(Button triggerButton, ImageView imageView) {
    triggerButton.setOnAction(event -> {
      try {
        System.out.println("Gamma Correction");

        TextInputDialog dialog = new TextInputDialog("1.5");
        dialog.setTitle("Gamma value selection");
        dialog.setHeaderText("Enter a gamma value for correction");
        dialog.setContentText("Value:");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {

          double gammaValue = Double.parseDouble(result.get());

          Image gammaCorrectedImage = ImageProcessor.processImage(
              imageView.getImage(),
              new GammaCorrectionLookupTable(
                  gammaValue
              )
          );

          imageView.setImage(gammaCorrectedImage);
        } else {
          throw new IllegalArgumentException("No value provided.");
        }
      } catch (IllegalArgumentException e) {

        Alert alert = new Alert(Alert.AlertType.ERROR);

        alert.setTitle("Error");
        alert.setHeaderText("An error occurred.");
        alert.setContentText(e.getMessage());

        alert.showAndWait();
      }
    });
  }
}
