package cz.hoffic.cs255.a1.action;

import cz.hoffic.cs255.a1.ImageProcessor;
import cz.hoffic.cs255.a1.helper.HistogramHelper;
import cz.hoffic.cs255.a1.helper.HistogramHelper.ChannelType;
import cz.hoffic.cs255.a1.lookuptable.HistogramEqualisationLookupTable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class HistogramEqualisationAction {

  public HistogramEqualisationAction(Button triggerButton, ImageView imageView) {
    triggerButton.setOnAction(event -> {
      System.out.println("Histogram equalisation.");

      Image equalisedImage = ImageProcessor.processImage(
          imageView.getImage(),
          new HistogramEqualisationLookupTable(
              HistogramHelper.getHistogramDistribution(imageView, ChannelType.BRIGHTNESS)
          )
      );

      imageView.setImage(equalisedImage);
    });
  }
}
