package cz.hoffic.cs255.a1.lookuptable;

import java.util.List;
import javafx.util.Pair;

public class ContrastLookupTable extends LookupTable {

  public ContrastLookupTable(List<Pair<Integer, Integer>> thresholds) {
    super();

    Pair lastThreshold = null;

    for (Pair threshold : thresholds) {
      if (lastThreshold == null) {
        lastThreshold = threshold;
        continue;
      }

      double r1 = (double) lastThreshold.getKey();
      double r2 = (double) threshold.getKey();
      double s1 = (double) lastThreshold.getValue();
      double s2 = (double) threshold.getValue();

      for (int intensity = (int) r1; intensity <= (int) r2; intensity++) {
        double result = (s2 - s1) / (r2 - r1) * (intensity - r1) + s1;

        table[intensity] = result / 255.0;
      }

      lastThreshold = threshold;
    }
  }
}
