package cz.hoffic.cs255.a1.lookuptable;

public class GammaCorrectionLookupTable extends LookupTable {

  public GammaCorrectionLookupTable(double gammaValue) {
    super();

    for (int i = 0; i < table.length; i++) {
      table[i] = Math.pow((i / 255.0), 1.0 / gammaValue);
    }
  }
}
