package cz.hoffic.cs255.a1.lookuptable;

public abstract class LookupTable {

  protected double[] table;

  protected LookupTable() {
    table = new double[256];
  }

  public double get(double intensity) {
    return table[(int) (intensity * 255)];
  }
}
