package cz.hoffic.cs255.a1.lookuptable;

public class HistogramEqualisationLookupTable extends LookupTable {

  public HistogramEqualisationLookupTable(int[] distribution) {
    super();

    int[] t = new int[256];

    for (int i = 0; i < t.length; i++) {
      if (i == 0) {
        t[i] = distribution[i];
      } else {
        t[i] = t[i - 1] + distribution[i];
      }
    }

    int size = t[t.length - 1];

    for (int i = 0; i < 256; i++) {
      table[i] = (1.0 * t[i] / size);
    }
  }
}
