package cz.hoffic.cs255.a1.lookuptable;

public class InvertLookupTable extends LookupTable {

  public InvertLookupTable() {
    super();

    for (int i = 0; i < table.length; i++) {
      table[i] = 1.0 - (i / 255.0);
    }
  }
}
