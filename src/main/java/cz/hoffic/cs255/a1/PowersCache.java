package cz.hoffic.cs255.a1;

public class PowersCache {

  private Double[] powers;

  public PowersCache(Double gammaValue) {
    powers = new Double[256];

    for (int i = 0; i < 256; i++) {
      powers[i] = Math.pow(i, gammaValue);
    }
  }

  public double getPower(int color) {
    return powers[color];
  }
}
