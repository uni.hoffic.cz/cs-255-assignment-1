package cz.hoffic.cs255.a1;

import cz.hoffic.cs255.a1.matrix.KernelMatrix;
import cz.hoffic.cs255.a1.lookuptable.LookupTable;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class ImageProcessor {

  private ImageProcessor() {
    throw new UnsupportedOperationException();
  }

  public static Image processImage(Image image, LookupTable lookupTable) {
    int width = (int) image.getWidth();
    int height = (int) image.getHeight();

    WritableImage outputImage = new WritableImage(width, height);
    PixelWriter outputImageWriter = outputImage.getPixelWriter();
    PixelReader imageReader = image.getPixelReader();

    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        Color color = imageReader.getColor(x, y);

        color = Color.color(
            lookupTable.get(color.getRed()),
            lookupTable.get(color.getGreen()),
            lookupTable.get(color.getBlue())
        );

        outputImageWriter.setColor(x, y, color);
      }
    }

    return outputImage;
  }

  public static Image greyScaleImgge(Image image) {
    int width = (int) image.getWidth();
    int height = (int) image.getHeight();

    WritableImage outputImage = new WritableImage(width, height);
    PixelWriter outputImageWriter = outputImage.getPixelWriter();
    PixelReader imageReader = image.getPixelReader();

    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        Color color = imageReader.getColor(x, y);

        double greyValue = (color.getRed() + color.getGreen() + color.getBlue()) / 3;

        color = Color.color(
            greyValue,
            greyValue,
            greyValue
        );

        outputImageWriter.setColor(x, y, color);
      }
    }

    return outputImage;
  }

  public static Image processImage(Image image, KernelMatrix kernelMatrixWrapper) {
    int width = (int) image.getWidth();
    int height = (int) image.getHeight();

    double[][] imageMatrixRed = new double[width][height];
    double[][] imageMatrixGreen = new double[width][height];
    double[][] imageMatrixBlue = new double[width][height];
    double[][] newImageMatrixRed = new double[width][height];
    double[][] newImageMatrixGreen = new double[width][height];
    double[][] newImageMatrixBlue = new double[width][height];
    int[][] kernelMatrix = kernelMatrixWrapper.getMatrix();

    PixelReader imageReader = image.getPixelReader();

    // transforming image to the image matrix
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        Color color = imageReader.getColor(x, y);
        imageMatrixRed[x][y] = color.getRed();
        imageMatrixGreen[x][y] = color.getGreen();
        imageMatrixBlue[x][y] = color.getBlue();
      }
    }

    double minValRed = Double.MAX_VALUE;
    double maxValRed = Double.MIN_VALUE;
    double minValGreen = Double.MAX_VALUE;
    double maxValGreen = Double.MIN_VALUE;
    double minValBlue = Double.MAX_VALUE;
    double maxValBlue = Double.MIN_VALUE;

    // calculating pixel value
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        double sumRed = 0;
        int count = 0;
        double sumGreen = 0;
        double sumBlue = 0;

        for (int my = 0; my < kernelMatrix.length; my++) {
          for (int mx = 0; mx < kernelMatrix[0].length; mx++) {

            if (x > 2 && y > 2 && x < width - 2 && y < height - 2) {
              sumRed += kernelMatrix[mx][my] * imageMatrixRed[x + mx - 2][y + my - 2];
              sumGreen += kernelMatrix[mx][my] * imageMatrixGreen[x + mx - 2][y + my - 2];
              sumBlue += kernelMatrix[mx][my] * imageMatrixBlue[x + mx - 2][y + my - 2];
              count++;
            }
          }
        }

        double correlatedValueRed = sumRed / count;
        double correlatedValueGreen = sumGreen / count;
        double correlatedValueBlue = sumBlue / count;

        newImageMatrixRed[x][y] = correlatedValueRed;
        newImageMatrixGreen[x][y] = correlatedValueGreen;
        newImageMatrixBlue[x][y] = correlatedValueBlue;

        if (correlatedValueRed < minValRed) {
          minValRed = correlatedValueRed;
        } else if (correlatedValueRed > maxValRed) {
          maxValRed = correlatedValueRed;
        }

        if (correlatedValueGreen < minValGreen) {
          minValGreen = correlatedValueGreen;
        } else if (correlatedValueGreen > maxValGreen) {
          maxValGreen = correlatedValueGreen;
        }

        if (correlatedValueBlue < minValBlue) {
          minValBlue = correlatedValueBlue;
        } else if (correlatedValueBlue > maxValBlue) {
          maxValBlue = correlatedValueBlue;
        }
      }
    }

    // normalisation
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        newImageMatrixRed[x][y] = (newImageMatrixRed[x][y] - minValRed) / (maxValRed - minValRed);
        newImageMatrixGreen[x][y] = (newImageMatrixGreen[x][y] - minValGreen) / (maxValGreen - minValGreen);
        newImageMatrixBlue[x][y] = (newImageMatrixBlue[x][y] - minValBlue) / (maxValBlue - minValBlue);
      }
    }

    WritableImage outputImage = new WritableImage(width, height);
    PixelWriter outputImageWriter = outputImage.getPixelWriter();

    // assigning calculated values to the image
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        Color color = Color.color(
            newImageMatrixRed[x][y],
            newImageMatrixGreen[x][y],
            newImageMatrixBlue[x][y]
        );

        outputImageWriter.setColor(x, y, color);
      }
    }

    return outputImage;
  }
}
