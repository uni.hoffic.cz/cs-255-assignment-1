package cz.hoffic.cs255.a1.helper;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;

public class HistogramHelper {

  public enum ChannelType {
    RED,
    GREEN,
    BLUE,
    BRIGHTNESS
  }

  public static int[] getHistogramDistribution(
      ImageView imageView,
      ChannelType channelType
  ) {
    int[] distribution = new int[256];

    Image image = imageView.getImage();

    int width = (int) image.getWidth();
    int height = (int) image.getHeight();

    PixelReader imageReader = image.getPixelReader();

    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        Color color = imageReader.getColor(x, y);

        double value;

        switch (channelType) {
          case RED:
            value = color.getRed();
            break;
          case GREEN:
            value = color.getGreen();
            break;
          case BLUE:
            value = color.getBlue();
            break;
          case BRIGHTNESS:
            value = (color.getRed() + color.getGreen() + color.getBlue()) / 3;
            break;
          default:
            throw new UnsupportedOperationException();
        }

        distribution[(int) (value * 255)]++;
      }
    }

    return distribution;
  }
}
