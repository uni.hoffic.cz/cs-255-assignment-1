package cz.hoffic.cs255.a1.matrix;

public class KernelMatrix {

  private int[][] matrix;

  public KernelMatrix(int[][] matrix) {
    this.matrix = matrix;
  }

  public int[][] getMatrix() {
    return matrix;
  }

  public static KernelMatrix getLaplacian() {
    int[][] matrix = {
        {-4, -1, 0, -1, -4},
        {-1, 2, 3, 2, -1},
        {0, 3, 4, 3, 0},
        {-1, 2, 3, 2, -1},
        {-4, -1, 0, -1, -4}
    };

    return new KernelMatrix(matrix);
  }
}
