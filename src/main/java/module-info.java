module cz.hoffic.cs255.a1 {
    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.web;
    requires javafx.swing;
    requires javafx.media;

    opens cz.hoffic.cs255.a1;
}
